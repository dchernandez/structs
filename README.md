## CSIS 113A: Examples of structs
***
Organization of data is a very important process in data processing.  

#
```
David C Hernandez, MSW, MBA
Associate Faculty, CSIS 
Mt San Jacinto College
```
#

### main.cpp
***
In this example, we look at the way to create simple arrays using bracket notation.  Notice that we utilize a similar 
pattern of declaration, so in order for the compiler to make sense of the difference between a regular variable and an 
array variable, the  **[]** is needed to delineate the difference.  

``` c++
// generation of functionals (example bind)
#include <functional> 

// for randomization in modern C++
#include <random>

```
