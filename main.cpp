/// David C Hernandez, MSW, MBA
/// GitHub link:
/// BitBucket link: https://bitbucket.org/dchernandez/structs/
/// struct lecture: Coin example
/// CSIS 113A C++ Level 1

/// Organization of data is critical for appropriate and efficient analysis.
/// In this section we will learn about grouping data of different types. Note that
/// while arrays/vectors allow us to group data, that data must be of the same type.
/// structs allow us to group like-data into a single object.
#include <iostream>
#include <cmath>

/// we can combine information for a coin into a single location to simply usage and
/// movements into functions. This is another way we can also return multiple values
/// from a function.
struct Location {
    int x;
    int y;
};

// Calculate Distance
//   determines the distance between two coordinate points
// @param from - the Location start point
// @param to - the Location to get to
// @return - a double representing the distance between the two points
double calculate_distance(const Location &from, const Location &to);

int main() {
    Location hero_location {3, 4};
    Location gold_location {0, 0};

    // display the distance between the two points
    std::cout << "Distance: " << calculate_distance(hero_location, gold_location) << std::endl;
    return 0;
}

double calculate_distance(const Location &from, const Location &to) {
    // find the difference between x and y coordinate distances
    int x_distance = from.x - to.x;
    int y_distance = from.y - to.y;

    // determine the sum of the distances.
    double squared_distance = pow(x_distance, 2) + pow(y_distance, 2);
    return sqrt(squared_distance);
}
