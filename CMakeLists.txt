cmake_minimum_required(VERSION 3.16)
project(Structs)

set(CMAKE_CXX_STANDARD 20)

add_executable(Structs main.cpp)